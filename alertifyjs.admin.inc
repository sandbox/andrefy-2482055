<?php
/**
 * @file
 * Administration page for AlertifyJS
 */

/**
 * Build general configurations form elements.
 */
function alertifyjs_admin_settings($form, &$form_state) {
  $alertifyjs_path = alertifyjs_libraries_get_path();

  if (!$alertifyjs_path) {
    drupal_set_message(t('The library could not be detected. You need to download the !alertifyJS
      and extract the entire contents of the archive into the %path directory on your server.',
    array(
      '!alertifyJS' => l(t('alertifyJS JavaScript file'), ALERTIFYJS_WEBSITE_URL),
      '%path' => 'sites/all/libraries'
      )
    ), 'error');
    return;
  }

  return system_settings_form($form);
}
